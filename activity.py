# 1. Create a Class called Camper and give it the attributes name, batch, course_type
class Camper():
    def __init__(zuitt_camper, name, batch, course_type):
        zuitt_camper.name = name
        zuitt_camper.batch = batch
        zuitt_camper.course_type = course_type
# 2. Create a method called career_track which will print out the string 'Currently enrolled in the <value of course_type> program'
    def career_track(zuitt_camper):
        print(f"Currently enrolled in the {zuitt_camper.course_type} program")
# 3. Create a method called info which will print out the string 'My name is <value of name> of batch <value of batch>.'
    def info(zuitt_camper):
        print(f"My name is {zuitt_camper.name} of batch {zuitt_camper.batch}.")
# 4. Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type
# 5. Print the value of the object's name
camper = Camper("Jarel", 295, "Python Short Course")
print(f"Camper Name: {camper.name}")
# 6. Print the value of the object's batch
print(f"Camper Batch: {camper.batch}")
# 7. Print the value of the object's course type
print(f"Camper Course: {camper.course_type}")
# 8. Execute the info method of the object
camper.info()
# 9. Execute the career_track method of the object
camper.career_track()